# ![](https://functorflow.org/img/logo.png)  Welcome to Forum FunctorFlow 

**Here you can request new plugins to develop, suggest your ideas, vote ideas, report issues and interact with other developers.**

> Rule: Upvote/Downvote to impact on the importance of the requests, ideas, issues etc


| [**The most requested plugins**](https://gitlab.com/functorflow/forum/issues?label_name%5B%5D=plugin-request&sort=popularity) | [**Recently requested plugins**](https://gitlab.com/functorflow/forum/issues?label_name%5B%5D=plugin-request&sort=created_date) |
| ------ | ------ |


<!--## Plugin requests-->
<!--- [Popular](https://gitlab.com/functorflow/forum/issues?label_name%5B%5D=plugin-request&sort=popularity)-->
<!--- [Recent](https://gitlab.com/functorflow/forum/issues?label_name%5B%5D=plugin-request&sort=created_date)-->


## How to Request a new plugin?

### 1. Public request 
- [Create new issue and label it as `plugin-request`](https://gitlab.com/functorflow/forum/issues/new)

### 2. Anonymous request (via email)
Send email to `plugin-request@functorflow.org`
- **Title:** Short description of a idea/plugin
- **Body:**  Tell us why it matters.


